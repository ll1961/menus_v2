class Order < ActiveRecord::Base
  has_many :line_items
  attr_accessible :address, :city, :email, :name, :order_id, :state, :status
  before_destroy :destroy_line_items

  def self.get_order(order_id)
    myorder = Order.find_by_order_id(order_id) if !order_id.nil?
    myorder
  end

  def self.set_order_id(params_order_id, current_order_id)
    if params_order_id
      order_id = params_order_id
    elsif current_order_id
      order_id = current_order_id
    end
    order_id
  end

  def self.submit_order(order_id)
    myorder = Order.get_order(order_id)
    myorder.status="Called In"
    myorder.save
    myorder
  end

  private

  def destroy_line_items
    @line_items = LineItem.find_all_by_order_id(self.order_id)
    @li_count = LineItem.find_all_by_order_id(self.order_id).count
    puts @line_items.inspect
    puts @li_count.inspect
    @line_items.each do |li|
      li.destroy
    end
  end

end
