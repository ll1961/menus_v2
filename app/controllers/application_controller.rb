class ApplicationController < ActionController::Base

  protect_from_forgery

  private

  def current_order
    session[:order_id]
  end

  helper_method :current_order
end
