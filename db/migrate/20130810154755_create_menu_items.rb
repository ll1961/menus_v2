class CreateMenuItems < ActiveRecord::Migration
  def change
    create_table :menu_items do |t|
      t.string :descr
      t.string :name
      t.float :price
      t.string :m_type
      t.string :meal
      t.timestamps
    end
  end
end
