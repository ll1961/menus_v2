class OrdersController < ApplicationController

  before_filter :authenticate_user!, :only => [:submit, :show, :destroy, :index]

  # GET /orders
  # GET /orders.json
  def index

    #   redirect_to user_session_url if !user_signed_in?
    if current_user.role.to_s.eql?("admin")
      @orders = Order.all
    else
      @orders = Order.find_all_by_email(current_user.email)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @orders }
    end
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
    @order = Order.find_by_order_id(params[:order_id])
    if !@order.status.blank?
      redirect_to "/line_items/index/#{@order.order_id}"
    else
      redirect_to "/orders/view/#{@order.order_id}"
    end

#    respond_to do |format|
#      format.html # show.html.erb
#      format.json { render json: @order }
#    end
  end

  # GET /orders/new
  # GET /orders/new.json
  def new
    session[:return_to] = request.referer
    @order = Order.new
    @order.order_id = session[:order_id]
    @user = User.find_by_email(current_user.email) if !current_user.nil?

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @order }
    end
  end

  # GET /orders/1/edit
  def edit
    @order = Order.find(params[:id]) if !params[:id].nil?
#    @line_items = LineItem.find_all_by_order_id(@order.order_id)
#    redirect_to "/line_items/index/#{@order.order_id}"
  end

  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new(params[:order])

    respond_to do |format|
      if @order.save
        #       format.html { redirect_to root_path(@order.order_id), notice: 'Order was successfully created.' }
        if !@order.status.eql?("")
          format.html { redirect_to "/line_items/index/#{@order.order_id}", notice: 'Order was successfully created.'}
        else
          format.html { redirect_to "/orders/view/#{@order.order_id}", notice: 'Order was successfully created.'}
        end
        format.json { render json: @menu_item, status: :created, location: @line_items }
      else
        format.html { render action: "new" }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /orders/1
  # PUT /orders/1.json
  def update
    @order = Order.find(params[:id])
    respond_to do |format|
      if @order.update_attributes(params[:order])
#        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.html { redirect_to "/line_items/index/#{@order.order_id}", notice: 'Order was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order = Order.find(params[:id])
    session[:order_id] = nil
    @order.destroy

    respond_to do |format|
#      format.html { redirect_to orders_url }
      format.html { redirect_to request.referer }
      format.json { head :no_content }
    end
  end

  def submit

    # redirect_to new_user_session_url if !user_signed_in?
    @order = Order.submit_order(session[:order_id])
    #@order = Order.find_by_order_id(session[:order_id])
    #@order.status="Called In"
    #@order.save
    #@order = Order.get_order(session[:order_id])
    #OrderMailer.order_submitted_email(@order).deliver
    reset_session
#    session[:order_id] = nil

    respond_to do |format|
      format.html { redirect_to root_url, notice: 'Order has been successfully submitted and will be processed.'   }
      format.json { head :no_content }
    end
  end

end
