MenusV2::Application.routes.draw do
  devise_for :users

  get '/users/sign_in/' => 'devise/sessions#new', :as => 'sign_in'

  resources :menu_items

  resources :orders
  get '/orders/index/:email' => 'orders#index', :constraints => { :email=> /[^\/]*/ }
  get '/orders/view/:order_id' => 'orders#view', :as => 'view_order'
  get '/orders/submit/:id' => 'orders#submit', :as => 'submit_order'

  resources :line_items, :except => :show
  get '/line_items/new/:order_id' => 'line_items#new'
  get '/line_items/index' => 'line_items#index'
  get '/line_items/index/:order_id' => 'line_items#index'
  get '/line_items/:order_id'  => 'line_items#index'
  get '/line_items' => 'line_items#index'
  #get '/line_items/index/:m_type' => 'line_items#index'
  #get '/line_items/index/order/:order_id' => 'line_items#index', :as => 'view_my_order'
  #get '/line_items/index/mtype/:m_type' => 'line_items#index', :as => 'add_to_order'


  root :to => "line_items#index"

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
