class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :order_id
      t.string :name
      t.string :address
      t.string :email
      t.string :city
      t.string :state

      t.timestamps
    end
  end
end
