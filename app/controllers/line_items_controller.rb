class LineItemsController < ApplicationController
  require 'securerandom'

  # GET /line_items
  # GET /line_items.json
  def index

    @menu_item = MenuItem.get_menu_items(params[:m_type])

    order_id = Order.set_order_id(params[:order_id], current_order)
    #if params[:order_id]
    #  order = params[:order_id]
    #elsif current_order
    #  order = current_order
    #end
    @order = Order.get_order(order_id)

#    if !current_order.blank?
    if !order_id.nil?
      @mi_li = MenuItem.get_line_items_menu_items_for_order(order_id)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @menu_items }
    end
  end

  # GET /line_items/new
  # GET /line_items/new.json
  def new
    @menu_item = MenuItem.find(params[:id])
    @line_item = LineItem.new

    if session[:order_id].nil?
      session[:order_id] = SecureRandom.hex(7).upcase
    end

    session[:return_to] = request.referer

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @line_item }
    end
  end

  # GET /line_items/1/edit
  def edit
    # POST /line_items
    @line_item = LineItem.find(params[:id])
    @menu_item = MenuItem.find(@line_item.menu_item_id)

    session[:return_to] = request.referer

  end

  def create
    @line_item = LineItem.new(params[:line_item])
    respond_to do |format|
      if @line_item.save
        format.html { redirect_to session[:return_to], notice: 'Line item was successfully created.' }
#        format.html { redirect_to line_items_index_path, :m_type => params[:mtype], notice: 'Line item was successfully created.' }
#        format.html { redirect_to request.referer, notice: 'Line item was successfully created.' }
        format.json { render json: @menu_item, status: :created, location: @line_item }
      else
        format.html { render action: "new" }
        format.json { render json: @line_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /line_items/1
  # PUT /line_items/1.json
  def update
    @line_item = LineItem.find(params[:id])

    respond_to do |format|
      if @line_item.update_attributes(params[:line_item])
#        format.html { redirect_to line_items_index_path, :m_type => params[:m_type], notice: 'Line item was successfully updated.' }
        format.html { redirect_to session[:return_to], notice: 'Line item was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @line_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /line_items/1
  # DELETE /line_items/1.json
  def destroy
    @line_item = LineItem.find(params[:id])
    @line_item.destroy

    respond_to do |format|
      format.html { redirect_to line_items_url }
      format.json { head :no_content }
    end
  end

end
