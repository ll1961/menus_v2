class OrderMailer < ActionMailer::Base
  default from: "loisgh@nyc.rr.com"

  def order_submitted_email(order)
    @order = order
    mail(to: @order.email,subject: "Your order #{@order.order_id} is being processed")
  end

end
