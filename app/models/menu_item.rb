class MenuItem < ActiveRecord::Base
  attr_accessible :descr, :m_type, :meal, :name, :price
  has_many :line_items

  def self.get_line_items_menu_items_for_order(current_order)
    MenuItem.includes(:line_items).where("line_items.order_id = '#{current_order}'")
  end

  private

  def self.find_all_types
    MenuItem.select('distinct m_type').order('m_type ASC').map{|a| a.m_type }
  end

  def self.get_menu_items(m_type)
    m_type = 'Appetizers' if m_type.blank?
    MenuItem.find_all_by_m_type(m_type)
  end

end
