class CreateLineItems < ActiveRecord::Migration
  def change
    create_table :line_items do |t|
      t.integer :menu_item_id
      t.string :order_id
      t.string :extras
      t.string :spec_inst

      t.timestamps
    end
  end
end
