class LineItem < ActiveRecord::Base
  belongs_to :order
  belongs_to :menu_items

  attr_accessible :extras, :menu_item_id, :order_id, :spec_inst
  attr_accessor :m_type

end
